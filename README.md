# Kart - Drupal Commerce Theme
Kart is a clean, elegant, modern and fully responsive Drupal 9, 10, 11 theme by Dev5.dev. This theme is Drupal commerce ready.

## THEME FEATURES
- Drupal 9.x, 10.x, 11.x compatible
- Drupal commerce ready.
- Fully responsive
- Clean & modern design
- HTML5 & CSS3
- Self hosted Google Fonts
- Multi-level drop down main menu
- Bootstrap font icons
- Inbuilt slider and banners.
- One column, two column, three column page layout.
- Customizable theme setting
- and more..

## Theme page
https://dev5.dev/theme/drupal/kart


## Theme Demo
https://kart.dev5.dev


## Documentaion
https://dev5.dev/doc/kart


## Demo Content
https://dev5.dev/doc/kart/demo-site-content


## Custom Shortcodes
https://dev5.dev/doc/kart/custom-shortcodes


## Block region
https://dev5.dev/doc/kart/block-regions


## Requirements
Kart theme does not require anything beyond Drupal 9 / 10 / 11 core to work.


## Installation
https://dev5.dev/doc/kart/install-kart-theme


## Configuration
Navigate to: Administration >> Appearance >> Settings >> kart


## maintainer
Current maintainer:
* Ravi Shekhar - https://www.drupal.org/u/ravis